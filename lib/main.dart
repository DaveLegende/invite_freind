import 'package:chats/constantes/constants.dart';
import 'package:chats/listes/list_images.dart';
import 'package:chats/listes/list_name.dart';
import 'package:chats/screens/profile.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _controller = TextEditingController();
  String filter;

  @override
  void initState() {
    super.initState();
    _controller.addListener(() {
      setState(() {
        filter = _controller.text;
      });
    });
  }

  List<bool> check = [true, false, true, true, false, false, false, false];

  @override
  Widget build(BuildContext context) {

    var longueur = MediaQuery.of(context).size.width * 0.13;

    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: appBarColor,
          centerTitle: true,
          title: Text("Invite Friends", style: TextStyle(color: Colors.white)),
          leading: IconButton(
            icon: Icon(
              Icons.keyboard_arrow_left,
              color: Colors.white,
              size: 30,
            ),
            onPressed: () {},
          ),
        ),
        body: Column(children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
            height: MediaQuery.of(context).size.width * 0.12,
            width: MediaQuery.of(context).size.width,
            color: appBarColor,
            child: TextField(
              controller: _controller,
              decoration: InputDecoration(
                prefixIcon: IconButton(
                    icon: Icon(Icons.search, color: Colors.white, size: 20),
                    onPressed: () {}),
                hintText: "Search",
                hintStyle: TextStyle(color: Colors.white),
                filled: true,
                fillColor: Colors.grey[400],
                enabledBorder: UnderlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: BorderSide(color: Colors.grey[400]),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderRadius: BorderRadius.circular(25),
                  borderSide: BorderSide(color: Colors.grey[400]),
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: ListView.builder(
                itemCount: 8,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      InkWell(
                        onTap: () {},
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.all(10),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          return popUp(index);
                                        },
                                        child: ClipOval(
                                          child: Image(
                                            fit: BoxFit.cover,
                                            image:
                                                AssetImage("${listImage[index]}"),
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.13,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.13,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(right: 15),
                                      ),
                                      Text("${listName[index]}",
                                          textScaleFactor: 1),
                                    ],
                                  ),
                                  Container(
                                    height: 35,
                                    child: Card(
                                        elevation: 5,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20)),
                                        color: (check[index] == false)
                                            ? Colors.green[300]
                                            : Colors.white,
                                        child: Container(
                                          child: Checkbox(
                                            activeColor: Colors.black,
                                              value: (check[index]),
                                              onChanged: (bool b) {
                                                setState(() {
                                                  check[index] = b;
                                                });
                                              }),
                                        )),
                                  )
                                ],
                              ),
                            ),
                            Row(
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: SizedBox(
                                    width: longueur+25,
                                  ),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: Container(
                                    padding: EdgeInsets.only(left: longueur+25),
                                    height: 1,
                                    color: Colors.grey[300],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
        ]),
      ),
    );
  }

  Future<Null> popUp(int index) async {
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return SimpleDialog(
          //title: Text("${listName[index]}"),
          children: [
            InkWell(
            onTap: () {
              setState(() {
                Navigator.push(context, MaterialPageRoute(
                  builder: (BuildContext context) {
                    return Profile(index: index);
                  },
                ));
              });
            },
            child: ClipRRect(
              child: Image(
                image: AssetImage("${listImage[index]}"),
                height: 160,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.cover,
              ),
            ),
          ),
          ]
        );
      },
    );
  }
}
