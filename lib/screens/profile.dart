import 'package:chats/listes/list_images.dart';
import 'package:chats/listes/list_name.dart';
import 'package:flutter/material.dart';

class Profile extends StatefulWidget {

  int index;

  Profile({this.index});

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          ClipRRect(
            child: Image(
              image: AssetImage("${listImage[widget.index]}"),
              fit: BoxFit.contain,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
            ),
            borderRadius: BorderRadius.circular(20),
          ),
          Positioned(
            top: 50,
            child: Text("${listName[widget.index]}", style: TextStyle(color: Colors.white, fontSize: 25),)
          ),
        ],
      ),
    );
  }
}